package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.User;

public interface IUserService {

    User findByUsername(String username);

    void createUser(final User user);

}
