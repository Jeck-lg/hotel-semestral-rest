package cz.vse.hotelsemestral.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date checkIn;
    private Date checkOut;
    @ManyToOne
    @JoinColumn(name = "room_id", insertable = true, updatable = true)
    @Fetch(FetchMode.JOIN)
    private Room room;
    private String state;
    @ManyToOne
    private Client client;

    public Reservation(Date checkIn, Date checkOut, Room room, String state, Client client) {
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.room = room;
        this.state = state;
        this.client = client;
    }

    public Reservation() {
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
