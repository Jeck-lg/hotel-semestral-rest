package cz.vse.hotelsemestral.entity;

public enum State {
    PENDING, ACCEPTED, REJECTED;
}
