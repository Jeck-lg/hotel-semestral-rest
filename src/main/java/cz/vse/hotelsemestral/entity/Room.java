package cz.vse.hotelsemestral.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Room {
    @Id
    private int id;
    private String name;
    private int price;
    @OneToMany
    private List <Reservation> reservations;

    public Room(int id, String name, int price, List<Reservation> reservations) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.reservations = reservations;
    }

    public Room() {
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return name.equals(room.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
