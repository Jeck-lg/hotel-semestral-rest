package cz.vse.hotelsemestral.controllers;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.logic.Methods;
import cz.vse.hotelsemestral.repository.RoomRepo;
import cz.vse.hotelsemestral.service.ClientService;
import cz.vse.hotelsemestral.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.reflect.Method;
import java.security.Principal;

@Controller
public class ReservationSummaryPageController {

    @Autowired
    RoomService roomService;

    @Autowired
    ClientService clientService;



    @GetMapping("/reservation-summary-page")
    public String reservationSummaryPage(@RequestParam(name="email", required=true) String  email,
                                         @RequestParam(name="name", required=true) String name,
                                         @RequestParam(name="dateOfBirth", required=true) String dateOfBirth,
                                         @RequestParam(name="number", required=true) String number,
                                         @RequestParam(name="roomName", required=true) String roomName,
                                         @RequestParam(name="checkIn", required=true) String checkIn,
                                         @RequestParam(name="checkOut", required=true) String checkOut,
                                         Model model, Principal principal){


        if(principal != null) {
            model.addAttribute("principal", "true");
            Client client = clientService.getClientByEmail(principal.getName());
            model.addAttribute("email", client.getEmail());
            model.addAttribute("name", client.getName());
            model.addAttribute("dateOfBirthFormatted", Methods.parseDateToString(client.getDateOfBirth()));
            model.addAttribute("dateOfBirth", Methods.parseDateToString(client.getDateOfBirth()));
            model.addAttribute("number", client.getNumber());
            model.addAttribute("checkIn", checkIn);
            model.addAttribute("checkOut", checkOut);
            model.addAttribute("checkInFormatted", Methods.formatDate(checkIn));
            model.addAttribute("checkOutFormatted", Methods.formatDate(checkOut));
            model.addAttribute("room", roomService.getRoomByName(roomName));
            return "reservationSummary";
        }
        model.addAttribute("principal", "false");
        model.addAttribute("email", email);
        model.addAttribute("name", name);
        model.addAttribute("dateOfBirthFormatted", Methods.formatDate(dateOfBirth));
        model.addAttribute("dateOfBirth", dateOfBirth);
        model.addAttribute("number", number);
        model.addAttribute("checkIn", checkIn);
        model.addAttribute("checkOut", checkOut);
        model.addAttribute("checkInFormatted", Methods.formatDate(checkIn));
        model.addAttribute("checkOutFormatted", Methods.formatDate(checkOut));
        model.addAttribute("room", roomService.getRoomByName(roomName));
        return "reservationSummary";
    }
}
