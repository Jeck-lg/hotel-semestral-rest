package cz.vse.hotelsemestral.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AvailableRoomsFilterPageController {

    @GetMapping("/available-rooms-filter")
    public String availableRoomsFilter(Model model){
        return "availableRoomsFilter";
    }

}
