package cz.vse.hotelsemestral.controllers;

import cz.vse.hotelsemestral.entity.Room;
import cz.vse.hotelsemestral.logic.Methods;
import cz.vse.hotelsemestral.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AvailableRoomsPageController {

    @Autowired
    private RoomService roomService;

    @GetMapping("/available-rooms")
    public String availableRooms(@RequestParam(name="checkIn", required=true) String  checkIn,
                                 @RequestParam(name="checkOut", required=true) String checkOut ,
                                 Model model) {

        List <Room> availableRooms = new ArrayList<Room>(
                roomService.getAvailableRooms(
                        Methods.parseStringToDate(checkIn),
                        Methods.parseStringToDate(checkOut)));

        model.addAttribute("checkIn", checkIn);
        model.addAttribute("checkOut",checkOut);
        model.addAttribute( "rooms", availableRooms);
        return "availableRooms";
    }


}
